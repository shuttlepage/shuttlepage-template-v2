import { Component, OnInit } from '@angular/core';
import { LocationsService } from '../locations.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  locations: any;

  constructor(
    private locationsService: LocationsService
  ) { }

  ngOnInit() {
    this.locations = this.locationsService.getLocations();
  }
  
}
