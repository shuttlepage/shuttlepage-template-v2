import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { LocationsService } from '../locations.service';

@Component({
  selector: 'app-state-page',
  templateUrl: './state-page.component.html',
  styleUrls: ['./state-page.component.scss']
})
export class StatePageComponent implements OnInit {
  cities: any;
  currentState: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private locationsService: LocationsService
  ) { }

  ngOnInit() {
    let currentState = this.route.snapshot.paramMap.get('state-name')
    this.cities = this.locationsService.getCitiesFor(currentState);
  }

  backToStates() {
    this.router.navigate(['/']);
  }

}
