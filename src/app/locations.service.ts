import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocationsService {

  locations = {
      oaklahoma: {
        displayName: "Oaklahoma",
        cities: [
          {
            name: "Duncan Area",
            subdomain: "duncan"
          },
          {
            name: "Lawton",
            subdomain: "lawton"
          }
        ]
      },
      texas: {
        displayName: "Texas",
        cities: [
          {
            name: "Wichita Falls",
            subdomain: "wichita-falls"
          }
        ]
      },
      colorado: {
        displayName: "Colorado",
        cities: [
          {
            name: "Montrose",
            subdomain: "montrose"
          }
        ]
      },
      florida: {
        displayName: "Florida",
        cities: [
          {
            name: "Santa Rosa Area",
            subdomain: "santa-rosa"
          }
        ]
      }
    };

  constructor() { }

  getLocations() {
    return this.locations;
  }

  getCitiesFor(state) {
    return this.locations[state]['cities'];
  }
}
