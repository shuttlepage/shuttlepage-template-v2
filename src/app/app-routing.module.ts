import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './home-page/home-page.component';
import { FaqPageComponent } from './faq-page/faq-page.component';
import { ContactPageComponent } from './contact-page/contact-page.component';
import { StatePageComponent } from './state-page/state-page.component';


const routes: Routes = [
  {
    path: "",
    component: HomePageComponent
  },
  {
    path: "faq",
    component: FaqPageComponent
  },
  {
    path: "contact",
    component: ContactPageComponent
  },
  {
    path: 'states/:state-name',
    component: StatePageComponent
  },
  {
    path: "**",
    redirectTo: ""
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
